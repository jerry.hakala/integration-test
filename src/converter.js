/**
 * padding hex component if necessary 
 * Hex component representation requires
 * two hexadecimal characters.
 * @param {string} comp 
 * @returns { string } two hexadecimal characters.
 */
const pad = (comp) => {
    let padded = comp;
    if (comp.length < 2) {
        padded = "0" + comp;
    } 
    return padded;
};
/**
 * RGB-to-HEX conversion
 * @param {number} r RED 0-255
 * @param {number} g GREEN
 * @param {number} b BLUE
 * @returns {string} in hex color format, e.g., "#00ff00" (green)
 */
export const rgb_to_hex = (r, g, b) => {
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return "#" + pad(HEX_RED) + pad(HEX_GREEN) + pad(HEX_BLUE);
};

export const hex_to_rgb = (HexInput) => {
    const hexString = HexInput.toString();
    const r = parseInt(hexString.slice(1, 3), 16);
    const g = parseInt(hexString.slice(3, 5), 16);
    const b = parseInt(hexString.slice(5, 7), 16);

    return { r, g, b };
};
